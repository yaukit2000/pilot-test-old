import React, { useState } from 'react';
import logo from './logo.svg';
import './App.css';
import { easing, tween, styler, keyframes, chain, Action, ColdSubscription } from 'popmotion';
import { Styler } from 'stylefire';

type Point = { x: number, y: number };
const RADIUS = window.innerWidth > window.innerHeight ? window.innerHeight / 2 : window.innerWidth / 2;
const ITEM_WIDTH = RADIUS / 6;
const CENTER = { x: window.innerWidth / 2, y: window.innerHeight / 2 }

const EASING_ARR = [
  easing.createExpoIn(1),
  easing.createBackIn(1),
  easing.createAnticipateEasing(1),
  easing.linear,
  easing.easeIn,
  easing.easeOut,
  easing.easeInOut,
  easing.circIn,
  easing.circOut,
  easing.circInOut,
  easing.backIn,
  easing.backOut,
  easing.backInOut,
  easing.anticipate,
]

let MAX_DURATION = 500;
let MIN_DURATION = 500;
let REF_DURATION = MAX_DURATION + MIN_DURATION;
const TOTAL_DURATION = 1000 * 60 * 2;

const BASE_ITEM_COUNT = 6;
const VAR_ITEM_COUNT = 9;

type Props = {}
type State = {
  // round: number,
  // itemCount: number,
}
export default class App extends React.PureComponent<Props, State> {
  private allBoxs: Styler[];
  private currentSubscriptions: ColdSubscription[];
  private activateBoxCount: number;
  private answer: HTMLElement;
  constructor(props: Props) {
    super(props);
    this.allBoxs = [];
    this.currentSubscriptions = [];
    this.answer = null as any;
    this.activateBoxCount = 0;
  }
  componentDidMount() {
    const circle = styler(document.getElementById('circle') as Element);
    this.answer = document.getElementById('answer') as HTMLElement;
    tween({
      from: { x: CENTER.x - RADIUS, y: CENTER.y - RADIUS },
      to: { x: CENTER.x - RADIUS, y: CENTER.y - RADIUS }
    }).start(circle.set)
    for (let i = 0; i < 15; i++) {
      const box = styler(document.getElementById(`box${i}`) as Element);
      this.allBoxs.push(box);
      tween({
        from: { x: -ITEM_WIDTH, y: -ITEM_WIDTH },
        to: { x: -ITEM_WIDTH, y: -ITEM_WIDTH },
      }).start(box.set)
    }
  }

  start = () => {
    this.answer.innerText = '-'
    for (let s of this.currentSubscriptions) {
      s.stop();
    }
    this.currentSubscriptions = [];
    const count = this.activateBoxCount = Math.round(Math.random() * VAR_ITEM_COUNT) + BASE_ITEM_COUNT;
    for (let i = 0; i < 15; i++) {
      if (i < count) {
        this.currentSubscriptions.push(
          chain(
            ...generateActionList()
          ).start(this.allBoxs[i].set)
        );
      } else {
        this.currentSubscriptions.push(
          tween({
            from: { x: -ITEM_WIDTH, y: -ITEM_WIDTH },
            to: { x: -ITEM_WIDTH, y: -ITEM_WIDTH },
          }).start(this.allBoxs[i].set)
        )
      }
    }
  }

  end = () => {
    this.answer.innerText = '' + this.activateBoxCount;
    for (let s of this.currentSubscriptions) {
      s.stop();
    }
    this.currentSubscriptions = [];
  }

  render() {
    return (
      <div className="App">
        <div id={'circle'} style={{ width: RADIUS * 2, height: RADIUS * 2, borderRadius: '50%', backgroundColor: 'black' }} />
        <div className='box' id={'box0'} key={'box0'} style={{ width: ITEM_WIDTH, height: ITEM_WIDTH, backgroundColor: 'green', borderRadius: '50%' }} />
        <div className='box' id={'box1'} key={'box1'} style={{ width: ITEM_WIDTH, height: ITEM_WIDTH, backgroundColor: 'green', borderRadius: '50%' }} />
        <div className='box' id={'box2'} key={'box2'} style={{ width: ITEM_WIDTH, height: ITEM_WIDTH, backgroundColor: 'green', borderRadius: '50%' }} />
        <div className='box' id={'box3'} key={'box3'} style={{ width: ITEM_WIDTH, height: ITEM_WIDTH, backgroundColor: 'green', borderRadius: '50%' }} />
        <div className='box' id={'box4'} key={'box4'} style={{ width: ITEM_WIDTH, height: ITEM_WIDTH, backgroundColor: 'green', borderRadius: '50%' }} />
        <div className='box' id={'box5'} key={'box5'} style={{ width: ITEM_WIDTH, height: ITEM_WIDTH, backgroundColor: 'green', borderRadius: '50%' }} />
        <div className='box' id={'box6'} key={'box6'} style={{ width: ITEM_WIDTH, height: ITEM_WIDTH, backgroundColor: 'green', borderRadius: '50%' }} />
        <div className='box' id={'box7'} key={'box7'} style={{ width: ITEM_WIDTH, height: ITEM_WIDTH, backgroundColor: 'green', borderRadius: '50%' }} />
        <div className='box' id={'box8'} key={'box8'} style={{ width: ITEM_WIDTH, height: ITEM_WIDTH, backgroundColor: 'green', borderRadius: '50%' }} />
        <div className='box' id={'box9'} key={'box9'} style={{ width: ITEM_WIDTH, height: ITEM_WIDTH, backgroundColor: 'green', borderRadius: '50%' }} />
        <div className='box' id={'box10'} key={'box10'} style={{ width: ITEM_WIDTH, height: ITEM_WIDTH, backgroundColor: 'green', borderRadius: '50%' }} />
        <div className='box' id={'box11'} key={'box11'} style={{ width: ITEM_WIDTH, height: ITEM_WIDTH, backgroundColor: 'green', borderRadius: '50%' }} />
        <div className='box' id={'box12'} key={'box12'} style={{ width: ITEM_WIDTH, height: ITEM_WIDTH, backgroundColor: 'green', borderRadius: '50%' }} />
        <div className='box' id={'box13'} key={'box13'} style={{ width: ITEM_WIDTH, height: ITEM_WIDTH, backgroundColor: 'green', borderRadius: '50%' }} />
        <div className='box' id={'box14'} key={'box14'} style={{ width: ITEM_WIDTH, height: ITEM_WIDTH, backgroundColor: 'green', borderRadius: '50%' }} />
        <div style={{ position: 'absolute', right: 10, bottom: 10, display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
          <button style={{ position: 'relative', width: 50, height: 25 }} onClick={this.start} >Start</button>
          <button style={{ position: 'relative', width: 50, height: 25 }} onClick={this.end} >End</button>
          <b id={'answer'}>-</b>
        </div>
        <div style={{ position: 'absolute', left: 10, bottom: 10, display: 'flex', flexDirection: 'column', }}>
          <label>
            MAX
            <input style={{ width: 50 }} inputMode='text' defaultValue={REF_DURATION} onChange={(e) => {
              REF_DURATION = parseInt(e.target.value);
              if (isNaN(REF_DURATION)) {
                REF_DURATION = MAX_DURATION + MIN_DURATION;
              }
              MAX_DURATION = REF_DURATION - MIN_DURATION;
            }} />
          </label>
          <label>
            MIN
          <input style={{ width: 50 }} inputMode='text' defaultValue={MIN_DURATION} onChange={(e) => {
              MIN_DURATION = parseInt(e.target.value);
              if (isNaN(MIN_DURATION)) {
                MIN_DURATION = 500;
              }
              REF_DURATION = MAX_DURATION + MIN_DURATION;
            }} />
          </label>
        </div>
      </div>
    );
  }
}

function generateActionList() {
  let remainDuration = TOTAL_DURATION;
  let lastPoint = randomPoint();
  const tweenArr: Action[] = [];
  let duration: number;
  let nextPoint: Point;
  while (remainDuration > 0) {
    if (remainDuration > REF_DURATION) {
      duration = randomDuration();
    } else {
      duration = remainDuration;
    }
    nextPoint = randomPoint();
    tweenArr.push(tween({
      from: { ...lastPoint },
      to: { ...nextPoint },
      duration,
      ease: randomEasing(),
    }))
    lastPoint = nextPoint;
    remainDuration -= duration;
  }
  return tweenArr;
}

function randomEasing() {
  return EASING_ARR[Math.round(Math.random() * EASING_ARR.length)]
}

function randomDuration() {
  return Math.random() * MAX_DURATION + MIN_DURATION;
}

function randomPoint(): Point {
  const angle = randomAngle();
  const radius = Math.random() * (RADIUS - ITEM_WIDTH);
  return {
    x: Math.cos(angle) * radius + CENTER.x - (ITEM_WIDTH / 2),
    y: Math.sin(angle) * radius + CENTER.y - (ITEM_WIDTH / 2),
  }
}

function randomAngle() {
  return Math.random() * Math.PI * 2;;
}
